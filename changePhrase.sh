#!/usr/bin/env bash
if [ $# -lt 2 ] #Warunek sprawdzajacy czy liczba argumentow jest mniejsza(-lt) niz 2 
then #To co wtswietli sie gdy zostanie spelniony warunek
echo "changePhrase - zmieniamy wskazaną w parametrze frazę na inną (również wskazaną w parametrze) w plikach znajdujących się w bieżącym katalogu i jego podkatalogach."
echo " Przykład:"
echo ' $ changePhrase "dupa" "***"'
exit; #koniec skryptu dla spelnionego warunku if, zeby dalej skrypt sie nie wykonywal gdy nie ma argumentow
fi # koniec ifa

FIND=${1:-dupa} #zmienna dla pierwszego argumentu, gdy argument jest pusty to wklei dupa
REPLACE=${2:-"***"} ##zmienna dla drugiego argumentu, gdy argument jest pusty to wklei sie ***
#generalnie nigdy nie przypisze sie ta dupa i *** ale dla czytelnosci kodu jest
DIR=${3:-"./"} #tym argumentem mozna ale nie trzeba zmienic katalog w ktorym szukamy plikow, domyslnie obecny katalog

find ${DIR} -type f -name "*" | xargs sed -i -e 's/'${FIND}'/'${REPLACE}'/g'
#powyzszy find w podanym katalogu szuka wszystkich plikow (tyoe f), nastepnie pipe przekazuje adresy plikow do xargs ktore uruchamiaja funkcje sed
#sed otwiera plik i szuka w nim podanej w pierwszym argumencie frazy zamieniajac ja na druga