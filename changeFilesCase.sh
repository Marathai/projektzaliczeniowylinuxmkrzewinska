#!/usr/bin/env bash
if [ $# -lt 2 ] #Warunek sprawdzajacy czy liczba argumentow jest mniejsza(-lt) niz 2 
then #To co wtswietli sie gdy zostanie spelniony warunek
echo '    * changeFilesCase - zmieniamy wielkość liter nazw plików znajdujących się w bieżącym katalogu. Sposób zamiany ma być podany w parametrze.'
echo '          Jeżeli nie zostanie podana nazwa pliku, przyjmuje się wszystkie pliki w katalogu.'
echo '          Przykład:'
echo '                $ changeFilesCase lower upper "test*"'
exit; #koniec skryptu dla spelnionego warunku if, zeby dalej skrypt sie nie wykonywal gdy nie ma argumentow
fi # koniec ifa

FROM=${1:-"upper"} #pierwszy zczytany argument
TO=${2:-"upper"} #drugi zczytany argument
#powyzsze uppery sie nie zczytaja ale jest dla czytelnosci kodu
FILE=${3:-'*'} #trzeci zczytany argument mowiacy jakich plikow ma szukac, domyslnie jest * oznaczajaca dowolny plik

for f in ${FILE} ; do mv -- "$f" "$(tr [:$FROM:] [:$TO:] <<< "$f")" ; done
#petla for szuka okreslonych plikow, i dla kazdego z nich zmienia nazwe.(mv)
#mv przyjmuje dwa parametry: plik zrodlowy oraz nowa nazwe pliku z zwiekszonymi/zmiejszonymi literkami przez program tr
#tr - zmienia jedna tresc na druga, tu uzywam go do zmieny wielkosci liter, tr na wejscu przyjmuje tresc ktora chce przetwazac a w argumentach pdaje jak ma zmieniac
#trzesc przyjmowana przez tr normalnie byla by z klawiatury, tutaj zamiast klawiatury uzywam <<< ktore oznaczaja wejscie