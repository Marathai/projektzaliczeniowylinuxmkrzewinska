#!/usr/bin/env bash
if [ "$1" == "-h" ]; #Warunek sprawdzajacy czy argument podany przez uzytkownika to "-h" 
then #To co wtswietli sie gdy zostanie spelniony warunek
echo "* setFileNumber - Skrypt numerujący wszystkie pliki w bieżącym katalogu. Wynik działania skryptu to zmiana nazwy plików na zaczynające się"
echo "      od numeru kolejnego + podkreślenie."
echo "      Przykład:"
echo "            1_jakistest"
echo "            2_test"
echo "            3_testinny"
exit; #koniec skryptu dla spelnionego warunku if, zeby dalej skrypt sie nie wykonywal gdy nie ma argumentow
fi # koniec ifa

COUNTER=1 #zmienna ustawiona na 1
for x in $(find * -maxdepth 0 -type f); do #for ktory, dla wszystkich wynikow zwroconych przez find (uzyty tu by zwrocil pliki (type f) z bierzacego katalogu, nie zaglebiajac sie (maxdepth 0)
        mv ${x} "${COUNTER}_${x}" #zmiana nazwy pliku (mv przyjmuje dwa parametry: plik zrodlowy oraz nowa nazwe pliku)
    let COUNTER=COUNTER+1 #zwiekszenie licznika o jeden by nastepna zmiana nazwy nastapila z wieksza liczba
done #koniec petli for